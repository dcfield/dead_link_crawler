# Python Web Crawler
This script will gather all dead links in a particular website.

- Call `python ./python_crawler/main.py`
- Enter a website to crawl (Default is [http://www.pythonanywhere.com/])
- Go for coffee
- Check `./dead_link_crawler/app/dead_link_crawler_files/dead.txt` for a list of all dead links